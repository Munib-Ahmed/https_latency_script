import math
import statistics
import numpy as np
import json
import re
import os
import time
import json
from datetime import datetime


#reading config.json
print("reading config.json")
config_file=open("config.json")
config=json.load(config_file)

time_to_record                  =config["time_to_record"]
application_ip                  =config["application_ip"]
server_ip                       =config["server_ip"]
tshark_path                     =config["tshark_path"]
certificate_file                =config["certificate_file"] 
server_port                     =config["server_port"] 

application_to_server_interface =config["observer_machine"]["application_to_server_interface"]
server_to_application_interface =config["observer_machine"]["server_to_application_interface"]


#setting directories
print("setting directories system")
path_of_e2e_perf_directory=os.path.dirname(os.path.abspath(__file__)) #Directory containing this script and key.pem
date_time=(datetime.now()).strftime("%d-%m-%Y_%H-%M-%S")
perf_run_directory=path_of_e2e_perf_directory+"/e2e-perf-runs/e2e-perf-"+date_time

if os.path.isdir(path_of_e2e_perf_directory+"/e2e-perf-runs"):
    pass
else:
    os.mkdir(path_of_e2e_perf_directory+"/e2e-perf-runs")

os.mkdir(perf_run_directory)
#Directories are created


#Recording pcaps
command="sudo tcpdump -U -i "+application_to_server_interface+ " src host "+application_ip+" -j adapter_unsynced --time-stamp-precision nano -w "+perf_run_directory+"/application-to-server.pcap &"
#print("Command: ",command)
os.system(command)

command="sudo tcpdump -U -i "+server_to_application_interface+" src host "+server_ip+" -j adapter_unsynced --time-stamp-precision nano -w "+perf_run_directory+"/server-to-application.pcap &"
#print("Command: ",command)
os.system(command)


#wait to record for specified time
time.sleep(time_to_record)

os.system("sudo pkill -2 tcpdump")
time.sleep(10)
#PCAPS recorded and tcpdump killed


#Merging pcaps files
command="mergecap -F nseclibpcap -w "+perf_run_directory+"/combined-e2e-run.pcap "+perf_run_directory+"/application-to-server.pcap "+perf_run_directory+"/server-to-application.pcap"
#print("Command: ",command)
os.system(command)
time.sleep(5)


#checking if merge is successfull
if os.path.isfile(perf_run_directory+"/combined-e2e-run.pcap")==False:
    print("File missing \"combined-e2e-run.pcap\"")
    exit(1)


#creating Decrypted file
command=tshark_path+ " -r "+ perf_run_directory+"/combined-e2e-run.pcap -V -x -o \"tls.desegment_ssl_records: TRUE\" -o \"tls.desegment_ssl_application_data: TRUE\" -o \"tls.keys_list:"+server_ip+","+server_port+",http,"+certificate_file +"\" > "+perf_run_directory+"/e2e-combined-decrypted.log"
#print("Command: ",command)
os.system(command)
time.sleep(5)


class frame_information: 
    def __init__(self):
        self.response_message = "" 
        self.response_epoch_time = 0
        self.request_message = ""
        self.request_epoch_time = 0
    
    def set_request_epoch_time(self, time):
        self.request_epoch_time=time
    def get_request_epoch_time(self):
        return self.request_epoch_time
        
    def set_response_epoch_time(self, time):
        self.response_epoch_time=time
    def get_response_epoch_time(self):
        return self.response_epoch_time
        
    def set_response_message(self, time):
        self.response_message=time
    def get_response_message(self):
        return self.response_message
        
    def set_request_message(self, time):
        self.request_message=time
    def get_request_message(self):
        return self.request_message

    def data_assignment_check(self):
        return ((self.response_message != "") and (self.response_epoch_time != 0) and (self.request_message!= "") and (self.request_epoch_time!=0)) 


with open(perf_run_directory+"/e2e-combined-decrypted.log") as f:
#with open("e2e-combined-decrypted.log") as f:
    Lines = f.readlines()
        

frame=""
frame_count = 1
list_extracted_data = {}
no_of_requests=0
no_of_response=0
no_of_sync=0
no_of_frames=0
extracted_message=""
print_line=0
print("iterating through the pcap file")


stats_file= open(perf_run_directory+'/e2e-perf-stats.log', 'w')
#stats_file= open('e2e-perf-stats.log', 'w')


for line in Lines:
    if "Epoch Time" in line:
        time=float(re.findall("\d+\.\d+", line)[0])
    elif "Decrypted TLS" in line:
        print_line = math.ceil(int(line[len("Decrypted TLS ("):-len(" bytes):")])/16)
        frame_type = ""
        first_line = True
        extracted_message=""
    elif print_line!=0:
        if first_line == True:
            if "HTTP/1.1 200 OK." in line:
                extracted_message = f'{extracted_message}{line[56:-1]}'
                frame_type = "response"
            elif "GET " in line or "POST " in line:
                extracted_message = f'{extracted_message}{line[56:-1]}'
                frame_type = "request"
        else:
            if frame_type== "response":
                extracted_message = f'{extracted_message}{line[56:-1]}'
            elif frame_type== "request":
                extracted_message = f'{extracted_message}{line[56:-1]}'

        first_line=False
        print_line = print_line - 1

        if print_line==0:
            stats_file.write("\n\n--------------------------")
            stats_file.write("\nFrame # "+ str(no_of_frames))
            no_of_frames=no_of_frames+1
            if extracted_message!="":
                if frame_type == "request":
                    stats_file.write("\nFrame Type: request")
                    no_of_requests=no_of_requests+1
                    json_request_message = json.loads(extracted_message.split()[1])
                    #print(frame_type, json_request_message["client_order_id"])
                    if json_request_message["client_order_id"] in list_extracted_data.keys():
                        list_extracted_data[(json_request_message["client_order_id"])].set_request_epoch_time(time)
                        list_extracted_data[(json_request_message["client_order_id"])].set_request_message(extracted_message)
                    else:
                        list_extracted_data[(json_request_message["client_order_id"])]=frame_information()   
                        list_extracted_data[(json_request_message["client_order_id"])].set_request_epoch_time(time)
                        list_extracted_data[(json_request_message["client_order_id"])].set_request_message(extracted_message)
                    stats_file.write("\nTime: "+ str(time))
                    stats_file.write("\nMessage: "+ str(extracted_message))
                elif frame_type == "response":
                    stats_file.write("\nFrame Type: response")
                    no_of_response=no_of_response+1
                    json_response_message = json.loads(extracted_message[(extracted_message.find("{")):])
                    #print(frame_type, json_response_message["client_order_id"])
                    if json_response_message["client_order_id"] in list_extracted_data.keys():
                        list_extracted_data[(json_response_message["client_order_id"])].set_response_epoch_time(time)
                        list_extracted_data[(json_response_message["client_order_id"])].set_response_message(extracted_message)
                    else:
                        list_extracted_data[(json_response_message["client_order_id"])]=frame_information()
                        list_extracted_data[(json_response_message["client_order_id"])].set_response_epoch_time(time)
                        list_extracted_data[(json_response_message["client_order_id"])].set_response_message(extracted_message)
                    stats_file.write("\nTime: "+ str(time))
                    stats_file.write("\nMessage: "+ str(extracted_message))
            else:
                no_of_sync=no_of_sync+1
                stats_file.write("\nFrame Type: syncronization message")
                stats_file.write("\nTime: "+ str(time))


print("list_extracted_data = ", len(list_extracted_data))
print("\n--------------------------")
print("total frames = ", no_of_frames)
print("total requests = ", no_of_requests)
print("total responses = ", no_of_response)
print("total syncronization(non req/res) messages = ", no_of_sync)
print("\n--------------------------")
time_difference=[]
print("Measuring latency") 


stats_file.write("\n--------------------------\n\n")
stats_file.write("total frames = "+ str(no_of_frames)+"\n")
stats_file.write("total requests = "+ str(no_of_requests)+"\n")
stats_file.write("total responses = "+ str(no_of_response)+"\n")
stats_file.write("total syncronization(non req/res) messages = "+ str(no_of_sync)+"\n")
stats_file.write("\n--------------------------\n")


for client_id in list_extracted_data:
    if list_extracted_data[client_id].data_assignment_check():
        time_difference.append(round((list_extracted_data[client_id].get_response_epoch_time()-list_extracted_data[client_id].get_request_epoch_time()),9))


time_difference.sort()

print("\n--------------------------")
print("Time:",date_time)
print("Duration:",time_to_record)
print("--------------------------")
print("Stats for: End-To-End")
print("--------------------------")
print("Mean :","%0.3f" %(float("%0.9f" %statistics.mean(time_difference))*1000000),"us")
print("Median :","%0.3f" %(float("%0.9f" %statistics.median(time_difference))*1000000),"us")
print("Min :", "%0.3f" %(float("%0.9f" %min(time_difference))*1000000) ,"us")
print("Max :", "%0.3f" %(float("%0.9f" %max(time_difference))*1000000) ,"us")
print("Standard Deviation :","%0.3f" %(float("%0.9f" %statistics.stdev(time_difference))*1000000),"us")
print("90th percentile :","%0.3f" %(float("%0.9f" %np.percentile(time_difference,90))*1000000),"us")
print("99th percentile :","%0.3f" %(float("%0.9f" %np.percentile(time_difference,99))*1000000),"us")
print("99.9th percentile :","%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.9))*1000000),"us")
print("99.99th percentile :","%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.99))*1000000),"us")
print("99.999th percentile :","%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.999))*1000000),"us")
print("Total Packet count :", len(time_difference))


result_file= open(perf_run_directory+'/e2e-perf-result.log', 'w')
result_file.write("--------------------------\n")
result_file.write("Time: "+date_time+"\n")
result_file.write("Duration: "+str(time_to_record)+"\n")
result_file.write("--------------------------\n")
result_file.write("Stats for: End-To-End\n")
result_file.write("--------------------------\n")
result_file.write("Mean : "+str("%0.3f" %(float("%0.9f" %statistics.mean(time_difference))*1000000))+" us\n")
result_file.write("Median : "+str("%0.3f" %(float("%0.9f" %statistics.median(time_difference))*1000000))+" us\n")
result_file.write("Min : "+ str("%0.3f" %(float("%0.9f" %min(time_difference))*1000000)) +" us\n")
result_file.write("Max : "+ str("%0.3f" %(float("%0.9f" %max(time_difference))*1000000)) +" us\n")
result_file.write("Standard Deviation : "+str("%0.3f" %(float("%0.9f" %statistics.stdev(time_difference))*1000000))+" us\n")
result_file.write("90th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,90))*1000000))+" us\n")
result_file.write("99th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99))*1000000))+" us\n")
result_file.write("99.9th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.9))*1000000))+" us\n")
result_file.write("99.99th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.99))*1000000))+" us\n")
result_file.write("99.999th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.999))*1000000))+" us\n")
result_file.write("Total Packet count: "+ str(len(time_difference)))


if len(time_difference) > 100:
    time_difference = time_difference[:len(time_difference)-100]


print("\n\nWithout top 100 outliers")
print("\n--------------------------")
print("Time: "+date_time)
print("Duration: ",time_to_record)
print("--------------------------")
print("Stats for: End-To-End")
print("--------------------------")
print("Mean : "+str("%0.3f" %(float("%0.9f" %statistics.mean(time_difference))*1000000))+" us")
print("Median : "+str("%0.3f" %(float("%0.9f" %statistics.median(time_difference))*1000000))+" us")
print("Min : "+ str("%0.3f" %(float("%0.9f" %min(time_difference))*1000000)) +" us")
print("Max : "+ str("%0.3f" %(float("%0.9f" %max(time_difference))*1000000)) +" us")
print("Standard Deviation : "+str("%0.3f" %(float("%0.9f" %statistics.stdev(time_difference))*1000000))+" us")
print("90th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,90))*1000000))+" us")
print("99th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99))*1000000))+" us")
print("99.9th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.9))*1000000))+" us")
print("99.99th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.99))*1000000))+" us")
print("99.999th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.999))*1000000))+" us")
print("Total Packet count: "+ str(len(time_difference)))

result_file.write("\n\nWithout top 100 outliers\n")
result_file.write("--------------------------\n")
result_file.write("Time: "+date_time+"\n")
result_file.write("Duration: "+str(time_to_record)+"\n")
result_file.write("--------------------------\n")
result_file.write("Stats for: End-To-End\n")
result_file.write("--------------------------\n")
result_file.write("Mean : "+str("%0.3f" %(float("%0.9f" %statistics.mean(time_difference))*1000000))+" us\n")
result_file.write("Median : "+str("%0.3f" %(float("%0.9f" %statistics.median(time_difference))*1000000))+" us\n")
result_file.write("Min : "+ str("%0.3f" %(float("%0.9f" %min(time_difference))*1000000)) +" us\n")
result_file.write("Max : "+ str("%0.3f" %(float("%0.9f" %max(time_difference))*1000000)) +" us\n")
result_file.write("Standard Deviation : "+str("%0.3f" %(float("%0.9f" %statistics.stdev(time_difference))*1000000))+" us\n")
result_file.write("90th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,90))*1000000))+" us\n")
result_file.write("99th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99))*1000000))+" us\n")
result_file.write("99.9th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.9))*1000000))+" us\n")
result_file.write("99.99th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.99))*1000000))+" us\n")
result_file.write("99.999th percentile : "+str("%0.3f" %(float("%0.9f" %np.percentile(time_difference,99.999))*1000000))+" us\n")
result_file.write("Total Packet count: "+ str(len(time_difference)))