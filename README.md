# E2E HTTPS Latency Measurement

A Python based Script for E2E HTTPS Latency Measurement.

## To Compile:

Navigate to the Directory containing script, enter the following command:

- `python3 e2e-https-calculation.py`

## Dependencies:

Following are the dependencies, required for the Script:

- `Config.json - containing the neccessary fields (most of them are self-explainatory)`
- `Server.pem - file containing server certificates, required for decryption (currently taken from the Huobi mock-server file)`

## Information:

- `More insights can be extracted from the Logging folder created specifically for this execution`
